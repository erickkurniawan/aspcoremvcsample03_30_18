﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleASPCoreMVC.Models
{
    public interface ICrud<T>
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetByID(string id);
        Task<T> Insert(T obj);
        Task<T> Edit(string id, T obj);
        Task<bool> Delete(string id);
    }
}
