﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SampleASPCoreMVC.Models;

namespace SampleASPCoreMVC.Controllers
{
    [Authorize]
    public class RestaurantsController : Controller
    {
        private IRestaurant _context;
        private ICategory _category;
        public RestaurantsController(IRestaurant context,ICategory category)
        {
            _context = context;
            _category = category;
        }

        // GET: Restaurants
        public async Task<IActionResult> Index()
        {
            return View(await _context.GetAll());
        }

        // GET: Restaurants/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurant = await _context.GetByID(id);
            if (restaurant == null)
            {
                return NotFound();
            }
            return View(restaurant);
        }

        [HttpPost]
        public async Task<IActionResult> Search(string search)
        {
            var results = await _context.GetByName(search);
            return View("Index", results);
        }


        // GET: Restaurants/Create
        [Authorize(Roles ="admin")]
        public async Task<IActionResult> Create()
        {
            ViewBag.SelectCategoryName = new SelectList(await _category.GetAll(), "CategoryID", "CategoryName");
            return View();
        }

        // POST: Restaurants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CategoryID,RestaurantName")] Restaurant restaurant, 
            IFormFile file)
        {
            if (ModelState.IsValid)
            {
                
                if (file == null || file.Length == 0)
                    return Content("file not selected");

                var filename = $"{Guid.NewGuid()}-{file.FileName}"; 
                var path = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/myimages",
                            filename);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                restaurant.Photo = filename;
                await _context.Insert(restaurant);

                return RedirectToAction(nameof(Index));
            }
            ViewBag.SelectCategoryName = new SelectList(await _category.GetAll(), "CategoryID", "CategoryName",
                    restaurant.CategoryID);
            return View(restaurant);
        }

        // GET: Restaurants/Edit/5
        [Authorize(Roles ="admin")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurant = await _context.GetByID(id);
            if (restaurant == null)
            {
                return NotFound();
            }
            ViewBag.SelectCategoryName = new SelectList(await _category.GetAll(), 
                "CategoryID", "CategoryName", restaurant.CategoryID);

            return View(restaurant);
        }

        // POST: Restaurants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("CategoryID,RestaurantName,Address")] Restaurant restaurant)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _context.Edit(id,restaurant);
                }
                catch (Exception ex)
                {
                    ViewBag.Pesan = "Error : " + ex.Message;
                }
                return RedirectToAction(nameof(Index));
            }
            ViewBag.SelectCategoryName = new SelectList(await _category.GetAll(),
                "CategoryID", "CategoryName", restaurant.CategoryID);
            return View(restaurant);
        }

        // GET: Restaurants/Delete/5
        [Authorize(Roles ="admin")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurant = await _context.GetByID(id);
            if (restaurant == null)
            {
                return NotFound();
            }

            return View(restaurant);
        }

        // POST: Restaurants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            try
            {
                var restaurant = await _context.Delete(id);
            }
            catch (Exception ex)
            {
                ViewBag.Pesan = "Error : " + ex.Message;
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return Content("file not selected");

            var path = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot",
                        file.FileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return RedirectToAction("Index");
        }


        public async Task<IActionResult> SaveChecked(string[] myCheckbox)
        {
            await _context.UpdateIsPublic(myCheckbox);
            return RedirectToAction("Index");
        }

        /*private bool RestaurantExists(int id)
        {
            return _context.Restaurants.Any(e => e.RestaurantID == id);
        }*/
    }
}
