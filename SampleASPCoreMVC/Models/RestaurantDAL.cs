﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;

namespace SampleASPCoreMVC.Models
{
    public class RestaurantDAL : IRestaurant
    {
        private RestaurantDataContext _context;
        public RestaurantDAL(RestaurantDataContext context)
        {
            _context = context;       
        }

        public async Task<bool> Delete(string id)
        {
            var result = await GetByID(id);
            if (result != null)
            {
                try
                {
                    _context.Restaurants.Remove(result);
                    await _context.SaveChangesAsync();
                    return true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            else
            {
                throw new Exception("Data Not Found !");
            }
        }

        public async Task<Restaurant> Edit(string id, Restaurant obj)
        {
            var restaurant = await GetByID(id);
            if (restaurant != null)
            {
                restaurant.CategoryID = obj.CategoryID;
                restaurant.RestaurantName = obj.RestaurantName;
                restaurant.Address = obj.Address;
                await _context.SaveChangesAsync();
                return restaurant;
            }
            else
            {
                throw new Exception("Data tidak ditemukan !");
            }
        }

        public async Task<IEnumerable<Restaurant>> GetAll()
        {
            //var results = await _context.Restaurants.OrderBy(r => r.RestaurantName).ToListAsync();
            var results = await (from r in _context.Restaurants.Include("Category")
                                orderby r.RestaurantName ascending
                                select r).ToListAsync();
            return results;
        }

        public async Task<Restaurant> GetByID(string id)
        {
            /*var result = await _context.Restaurants.Include(r=>r.Category)
                .Where(r => r.RestaurantID.ToString() == id).SingleOrDefaultAsync();*/
            var result = await (from r in _context.Restaurants.Include(r=>r.Category)
                                where r.RestaurantID.ToString() == id
                                select r).SingleOrDefaultAsync();

            if (result != null)
                return result;
            else
                throw new Exception("Data tidak ditemukan !");
        }

        public async Task<IEnumerable<Restaurant>> GetByName(string name)
        {
            /*var results = await _context.Restaurants.Include(r => r.Category)
                .Where(r => r.RestaurantName.ToLower().Contains(name)).ToListAsync();*/
            var results = await (from r in _context.Restaurants.Include(r => r.Category)
                                 where r.RestaurantName.ToLower().Contains(name.ToLower())
                                 select r).ToListAsync();
            return results;
        }

        public async Task<Restaurant> Insert(Restaurant obj)
        {
            try
            {
                _context.Restaurants.Add(obj);
                await _context.SaveChangesAsync();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task UpdateIsPublic(string[] isPublicID)
        {
            //update semua menjadi false
            var results = _context.Restaurants;
            foreach(var r in results)
            {
                r.IsPublic = false;
            }

            foreach(var myId in isPublicID)
            {
                var result = _context.Restaurants
                    .Where(r => r.RestaurantID.ToString() == myId).SingleOrDefault();
                if (result != null)
                {
                    result.IsPublic = true;
                }
            }
            await _context.SaveChangesAsync();
        }
    }
}
