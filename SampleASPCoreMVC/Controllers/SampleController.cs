﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SampleASPCoreMVC.Controllers
{
    [Produces("application/json")]
    [Route("api/Sample")]
    public class SampleController : Controller
    {
        [HttpGet]
        public IEnumerable<string> Get()
        {
            string[] strName = new string[] { "Erick", "Budi", "Bambang", "Joko", "Jono" };
            return strName;
        }

        [HttpGet("{id}")]
        public string Get(string id)
        {
            return "Nama anda : " + id;
        }
    }
}