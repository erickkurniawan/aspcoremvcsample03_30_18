﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleASPCoreMVC.Models
{
    public class CategoryDAL : ICategory
    {
        private RestaurantDataContext _context;
        public CategoryDAL(RestaurantDataContext context)
        {
            _context = context;
        }

        public async Task<bool> Delete(string id)
        {
            var result = await GetByID(id);
            if (result != null)
            {
                _context.Categories.Remove(result);
                await _context.SaveChangesAsync();
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<Category> Edit(string id, Category obj)
        {
            var result = await GetByID(id);
            if (result != null)
            {
                result.CategoryName = obj.CategoryName;
                _context.SaveChanges();
                return result;
            }
            else
            {
                throw new Exception("Data tidak ditemukan !");
            }
        }

        public async Task<IEnumerable<Category>> GetAll()
        {
            /*var resultsLambda = await _context.Categories.OrderBy(c => c.CategoryName).Select(c => new Category
            {
                CategoryID = c.CategoryID,
                CategoryName = c.CategoryID + " - " + c.CategoryName
            }).ToListAsync();*/

            var results = await (from c in _context.Categories
                                orderby c.CategoryName ascending
                                select new Category
                                {
                                    CategoryID = c.CategoryID,
                                    CategoryName = c.CategoryID + " - "+c.CategoryName
                                }).ToListAsync();
            return results;
        }

        public async Task<Category> GetByID(string id)
        {
            var result = await (_context.Categories
                .Where(c => c.CategoryID.ToString() == id)).SingleOrDefaultAsync();
            return result;
        }

        public async Task<Category> Insert(Category obj)
        {
            try
            {
                _context.Add(obj);
                await _context.SaveChangesAsync();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
