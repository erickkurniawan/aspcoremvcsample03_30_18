﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SampleASPCoreMVC.Models;

namespace SampleASPCoreMVC.Controllers
{
    [Produces("application/json")]
    [Route("api/Category")]
    public class CategoryController : Controller
    {
        
        private ICategory _category;
        public CategoryController(ICategory category)
        {
            _category = category;
        }

        // GET: api/Category
        [HttpGet]
        public async Task<IEnumerable<Category>> GetCategory()
        {
            var results = await _category.GetAll();
            return results;
        }

        // GET: api/Category/5
        [HttpGet("{id}")]
        public async Task<Category> GetCategory(string id)
        {
            var result = await _category.GetByID(id);
            return result;
        }
        
        // POST: api/Category
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Category category)
        {
            try
            {
                await _category.Insert(category);
                return Ok("Insert Data Category Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        // PUT: api/Category/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, [FromBody]Category category)
        {
            try
            {
                await _category.Edit(id, category);
                return Ok("Update Data Category Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                await _category.Delete(id);
                return Ok("Delete data success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
