﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleASPCoreMVC.Models
{
    public class MyCheckViewModel
    {
        public List<CheckboxModel> Checkboxes { get; set; }
    }
}
