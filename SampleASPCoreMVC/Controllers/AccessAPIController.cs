﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using SampleASPCoreMVC.Models;

namespace SampleASPCoreMVC.Controllers
{
    public class AccessAPIController : Controller
    {
       

        public IActionResult Index()
        {
            var client = new RestClient("http://localhost:55536/");
            var request = new RestRequest("api/Restaurant", Method.GET);
            request.AddHeader("Authorization", 
                "Bearer EDfgO34PXHsqs1SdqR6aNag8P7FdwjuuKm7-Pgbkzz6qrpt755boZkjc8kphvlGNU7KwYw6r1TtvbLCs7K8XfeC5CxdMw5iRxDjKaazDligP18xIKb5K4G2y76T0oS0QrI5GTN50D2b98jGyiNhcsCzSCCMcjyV3gA_f8A0RAQuMkPQtCHe6Z3Ye-DnON26xZI5Vr6091vlei-gbzUrhgLl3qNzuSD-OffeWt0CNsCzPIGTxmrZwafhF0FbuHRdMurAeJZ5KRzgihEcXEpLGenmZ0V6DNe0Z5qiIkaGnE7LX1Xr38RgXHIwdO3Akh9gNoBGOvpUJafYuT-lfyN9KNYaSES-2GLx-hdXciajwbZ_gH9z7cmGqwyNpu9bdPz2jZgTsXadun4-ugk0Oiz7tygv0mhAIO-lckDsytyH__i-09O0sZ7MUNKm5FiO1ry-36tNME-dVn1-YE0lcNRXSoL6TmUJOcMZkeCSSM1bpw-Cc-VWqAhqn-1lTiiE2Zbiy");

            var response = client.Execute<List<Restaurant>>(request);
            //IRestResponse response = client.Execute(request);
            var model = response.Data;

            //return new ObjectResult(model);
            return View(model);
        }

        public IActionResult JSclientSample()
        {
            return View();
        }
    }
}