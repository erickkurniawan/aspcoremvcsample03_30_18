﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SampleASPCoreMVC.Models
{
    //dotnet ef migrations add TambahFieldPhotoIsPublic_Restaurant --context RestaurantDataContext
    public class Restaurant
    {
        [Display(Name ="Restaurant ID")]
        public int RestaurantID { get; set; }

        [Display(Name ="Category ID")]
        [Required(ErrorMessage ="Please select one Category")]
        public int CategoryID { get; set; }

        [Display(Name ="Restaurant Name")]
        [Required(ErrorMessage ="Restaurant Name Required")]
        public string RestaurantName { get; set; }

        public string Address { get; set; }
        public string Photo { get; set; }
        public bool IsPublic { get; set; }
        public Category Category { get; set; }
    }
}
