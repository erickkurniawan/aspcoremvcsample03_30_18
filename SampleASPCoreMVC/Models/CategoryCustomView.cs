﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleASPCoreMVC.Models
{
    public class CategoryCustomView
    {
        public int CategoryID { get; set; }
        public string CustomName { get; set; }
    }
}
