﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SampleASPCoreMVC.Migrations
{
    public partial class TambahField_Photo_isPublic_Restaurant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Address",
                table: "Restaurants",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsPublic",
                table: "Restaurants",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Photo",
                table: "Restaurants",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPublic",
                table: "Restaurants");

            migrationBuilder.DropColumn(
                name: "Photo",
                table: "Restaurants");

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                table: "Restaurants",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
