﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleASPCoreMVC.Models
{
    public interface IRestaurant : ICrud<Restaurant>
    {
        Task<IEnumerable<Restaurant>> GetByName(string name);
        Task UpdateIsPublic(string[] isPublicID);
    }
}
