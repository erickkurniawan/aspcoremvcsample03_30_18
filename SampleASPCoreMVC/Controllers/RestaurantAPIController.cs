﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SampleASPCoreMVC.Models;

namespace SampleASPCoreMVC.Controllers
{
    [Produces("application/json")]
    [Route("api/RestaurantAPI")]
    public class RestaurantAPIController : Controller
    {
        private IRestaurant _restaurant;
        public RestaurantAPIController(IRestaurant restaurant)
        {
            _restaurant = restaurant;
        }

        // GET: api/RestaurantAPI
        [HttpGet]
        public async Task<IEnumerable<Restaurant>> Get()
        {
            var results = await _restaurant.GetAll();
            return results;
        }

        // GET: api/RestaurantAPI/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }
        
        // POST: api/RestaurantAPI
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/RestaurantAPI/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
